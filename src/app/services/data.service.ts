import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Ticket} from '../models/Ticket';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';

@Injectable()
export class DataService {
  private readonly API_URL = 'http://localhost:8080/api/tickets/';

  dataChange: BehaviorSubject<Ticket[]> = new BehaviorSubject<Ticket[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor (private httpClient: HttpClient) {}

  get data(): Ticket[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  /** CRUD METHODS */
  getAllTickets(): void {
    this.httpClient.get<Ticket[]>(this.API_URL).subscribe(data => {
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
      console.log (error.name + ' ' + error.message);
      });
  }
  addTickets (ticket: Ticket): void {
    // tslint:disable-next-line:one-line
    this.httpClient.post(this.API_URL, ticket).subscribe(data => {
      console.log(data);
    });
    this.dialogData = ticket;
  }

  updateIssue (ticket: Ticket): void {
    this.httpClient.put(this.API_URL + 'update/', ticket).subscribe(
      data => {
        console.log(data);
      }
    );
    this.dialogData = ticket;
  }

  deleteIssue (ticketNumber: String): void {
    this.httpClient.delete(this.API_URL + ticketNumber).subscribe(data => {
      console.log(data);
    });
    console.log(ticketNumber);
  }
}




