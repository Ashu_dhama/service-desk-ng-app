export class Ticket {
  id: string;
  title: string;
  status: number;
  ticketNumber: string;
  problemDescription: string;
  priorityLevel: number;
  email: string;
  createdOn: string;
}
