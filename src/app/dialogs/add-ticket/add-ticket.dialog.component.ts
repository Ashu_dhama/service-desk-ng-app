import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject} from '@angular/core';
import {DataService} from '../../services/data.service';
import {FormControl, Validators} from '@angular/forms';
import {Ticket} from '../../models/Ticket';

@Component({
  selector: 'app-add.dialog',
  templateUrl: './/add-ticket.dialog.component.html',
  styleUrls: ['.//add-ticket.dialog.component.css']
})

export class AddTicketDialogComponent {
  constructor(public dialogRef: MatDialogRef<AddTicketDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Ticket,
              public dataService: DataService) {}

  formControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }

  submit() {
  // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.dataService.addTickets(this.data);
  }
}
